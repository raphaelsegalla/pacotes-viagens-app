package br.com.aluraviagens.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class Pacote implements Serializable {

    private final String local;
    private final String imagens;
    private final int dias;
    private final BigDecimal preco;

    public Pacote(String local, String imagens, int dias, BigDecimal preco) {
        this.local = local;
        this.imagens = imagens;
        this.dias = dias;
        this.preco = preco;
    }

    public String getLocal() {
        return local;
    }

    public String getImagens() {
        return imagens;
    }

    public int getDias() {
        return dias;
    }

    public BigDecimal getPreco() {
        return preco;
    }
}
